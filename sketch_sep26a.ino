#include <Arduino.h>
#include <string.h>


//default delay and output trigger length

int length = 4000;

int wholes = 0;
int remain =5000-8;


const byte outPinA = 8;
const byte outPinB = 9;

unsigned long int trig_delay=0;

void setup() {
  pinMode(outPinA, OUTPUT);
  pinMode(outPinB, OUTPUT);



  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.println("Welcome to arduino trigger system for GOLEM");

}


void loop() {


  if (Serial.available() > 0) {
          // read the incoming line:
          String incoming;
          incoming = Serial.readString();
           
          if (incoming.substring(0, 3) == "set"){
              String text = incoming.substring(4);
              trig_delay = text.toInt();
          }

          else if (incoming.substring(0,1) == "t"){

              if (trig_delay==0){
                //both should be fired at the same time

                  //digitalWrite was slow
                  //digitalWrite(outPin, HIGH);
                  PORTB = B000011;
                  delayMicroseconds(length);
                  PORTB = B000000;
              }
             
              else{
                //ok, now we have 2nd channel delayed
                
                wholes=trig_delay/15000;
                remain=trig_delay%15000;


                 noInterrupts();

                  PORTB = B000001;
                  for(int i = 1; i<=wholes; i++)
                    delayMicroseconds(15000);
                  delayMicroseconds(remain);
                  PORTB += B000010;
                  delayMicroseconds(length);
                  PORTB = B000000;
                interrupts();
              }
            }

           else{
             Serial.println("unknown command");
             Serial.println(incoming);
           }
    //delay(500);
     }

}
